//
//  ARDelegate.swift
//  DRAWABLE
//
//  Sets ARView – configuration, gestures
//  Creates plane with diffuse material set to picked image
//  Inserts created plane based on user's tap
//  Handles interractions with augmented image - rotation, resize, move, opacity
//
//  Created by Adela Mišicáková on 31.01.2022.
//

import Foundation
import ARKit
import UIKit

class ARDelegate: NSObject, ARSCNViewDelegate {
    
    var arView: ARSCNView?
    var addedPlane = SCNNode()
    var planeAdded = false
    
    //MARK: - Set ARview
    
    func setARView(_ arView: ARSCNView, imageToAugment: UIImage) {
        self.arView = arView
        
        // Coaching animation
        arView.addCoaching()
        
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal,.vertical]
        arView.session.run(configuration)
        
        arView.delegate = self
        arView.scene = SCNScene()
        
        let tapGestureRecognizer = TapGestureRecognizerWithImage(target: self, action: #selector(handleTapOnARView))
        tapGestureRecognizer.image = imageToAugment
        arView.addGestureRecognizer(tapGestureRecognizer)
        
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePanOnARView))
        arView.addGestureRecognizer(panGestureRecognizer)
        
        let rotationGestureRecognizer = UIRotationGestureRecognizer(target: self, action: #selector(handleRotationOnARView))
        arView.addGestureRecognizer(rotationGestureRecognizer)
        
        let resizeGestureRecognizer = UIPinchGestureRecognizer(target: self, action: #selector(handleResizeOnARView))
        arView.addGestureRecognizer(resizeGestureRecognizer)
    }
    
    //MARK: - Tap gesture handler
    
    @objc func handleTapOnARView(sender: TapGestureRecognizerWithImage) {
        if planeAdded == false {
            guard let arView = arView else { return }
            let location = sender.location(in: arView)
            let imageToAugment = sender.image
            
            if let result = raycastResultHorizontal(fromLocation: location) {
                let position = result.worldTransform
                placePlane(position: position, imageToAugment: imageToAugment, isHorizontal: true)
                planeAdded = true
            } else if let result = raycastResultVertical(fromLocation: location) {
                let position = result.worldTransform
                placePlane(position: position, imageToAugment: imageToAugment, isHorizontal: false)
                planeAdded = true
            }
        }
    }
    
    //MARK: - Pan gesture handler
    
    @objc func handlePanOnARView(sender: UIPanGestureRecognizer) {
        guard let arView = arView else { return }
        let location = sender.location(in: arView)
        let originalEuler = addedPlane.eulerAngles
        
        if let result = raycastResultHorizontal(fromLocation: location) {
            let position = result.worldTransform
            addedPlane.position = SCNVector3(x: Float(position.columns.3.x), y: Float(position.columns.3.y), z: Float(position.columns.3.z))
            addedPlane.eulerAngles = SCNVector3(-(.pi/2), originalEuler.y, 0)
        } else if let result = raycastResultVertical(fromLocation: location) {
            let position = result.worldTransform
            addedPlane.position = SCNVector3(x: Float(position.columns.3.x), y: Float(position.columns.3.y), z: Float(position.columns.3.z))
            addedPlane.eulerAngles = SCNVector3(0, 0, originalEuler.z)
        }
    }
    
    //MARK: - Rotation gesture handler
    
    @objc func handleRotationOnARView(sender: UIRotationGestureRecognizer) {
        let rotation = sender.rotation
    
        if addedPlane.eulerAngles.x != 0 {
            //horizontal plane rotation
            addedPlane.eulerAngles = SCNVector3(-(.pi/2), -rotation, 0)
        } else {
            // vertical plane rotation
            addedPlane.eulerAngles = SCNVector3(0, 0, -rotation)
        }
    }
    
    //MARK: - Resize gesture handler
    
    @objc func handleResizeOnARView(sender: UIPinchGestureRecognizer) {
        let resize = sender.scale
        addedPlane.scale = SCNVector3(x: Float(resize), y: Float(resize), z: Float(resize))
    }
    
    //MARK: - Update plane opacity
    
    func updatePlaneOpacity(imageOpacity: Double) {
        addedPlane.opacity = imageOpacity
    }
    
    //MARK: - Raycast Horizontal
    
    private func raycastResultHorizontal(fromLocation location: CGPoint) -> ARRaycastResult? {
        guard let arView = arView,
              let query = arView.raycastQuery(from: location, allowing: .existingPlaneGeometry, alignment: .horizontal) else { return nil }
        let results = arView.session.raycast(query)
        return results.first
    }
    
    //MARK: - Raycast Vertical
    
    private func raycastResultVertical(fromLocation location: CGPoint) -> ARRaycastResult? {
        guard let arView = arView,
              let query = arView.raycastQuery(from: location, allowing: .existingPlaneGeometry, alignment: .vertical) else { return nil }
        let results = arView.session.raycast(query)
        return results.first
    }
    
    //MARK: - Place plane
    
    func placePlane(position: simd_float4x4, imageToAugment: UIImage, isHorizontal: Bool) {
        let plane = createPlane(imageToAugment: imageToAugment)
        plane.transform = SCNMatrix4(position)
        
        if isHorizontal {
            plane.rotation = SCNVector4Make(1, 0, 0, -(.pi/2))
        } else {
            plane.rotation = SCNVector4Make(0, 0, 0, 0)
        }
        
        arView?.scene.rootNode.addChildNode(plane)
        addedPlane = plane
    }
    
    //MARK: - Create plane with image
    
    func createPlane(imageToAugment: UIImage) -> SCNNode {
        let material = SCNMaterial()
        material.diffuse.contents = imageToAugment
        
        let aspectRatio = imageToAugment.size.height / imageToAugment.size.width
        let newWidth = 0.1
        let newHeight = newWidth * aspectRatio
        
        let planeGeometry = SCNPlane(width: newWidth, height: newHeight)
        planeGeometry.materials = [material]
        
        let planeNode = SCNNode(geometry: planeGeometry)
        return planeNode
    }
}

class TapGestureRecognizerWithImage: UITapGestureRecognizer {
    var image = UIImage()
}
