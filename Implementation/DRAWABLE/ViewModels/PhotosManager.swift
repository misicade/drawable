//
//  PhotosManager.swift
//  DRAWABLE
//
//  Handles photos fetching from Unsplash API
//
//  Created by Adela Mišicáková on 19.03.2022.
//

import Foundation

class PhotosManager : ObservableObject {
    
    @Published var photos = [Photo]()
    
    let photosURL = "https://api.unsplash.com/search/photos?"
    let client_id = "kq8g3YfgvVu9UltaKprjoJwOvbt8Uz9LHlm4uuQfvp8"
    
    //MARK: - Fetch photo
    
    func fetchPhoto (photoName: String, numOfPhotos: Int) {
        photos.removeAll()
        let safeName = photoName.replacingOccurrences(of: " ", with: "")
        let urlString = "\(photosURL)query=\(safeName)&per_page=\(numOfPhotos)&client_id=\(client_id)"
        performRequest(urlString: urlString)
    }
    
    //MARK: - Perform request
    
    func performRequest(urlString: String) {
        if let url = URL(string: urlString) {
            let session = URLSession(configuration: .default)
            
            let task = session.dataTask(with: url) { data, response, error in
                if error != nil {
                    print(error!)
                    return
                }
                
                if let safeData = data {
                    if let safePhotos = self.parseJSON(photoData: safeData) {
                        DispatchQueue.main.sync {
                            self.photos.append(contentsOf: safePhotos.results)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    //MARK: - Parse JSON
    
    func parseJSON(photoData: Data) -> PhotoData? {
        let decoder = JSONDecoder()
        
        do {
            let decodedData = try decoder.decode(PhotoData.self, from: photoData)
            return decodedData
        } catch {
            print(error)
            return nil
        }
    }
}
