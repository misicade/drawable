//
//  ARViewContainer.swift
//  DRAWABLE
//
//  Handles ARView
//
//  Extension of ARSCNView with addCoaching (adds ARCoachingOverlayView)
//
//  Created by Adela Mišicáková on 31.01.2022.
//

import ARKit
import SwiftUI

struct ARViewContainer: UIViewRepresentable {
    
    var imageToAugment: UIImage
    var arDelegate: ARDelegate
    
    func makeUIView(context: Context) -> ARSCNView {
        let arView = ARSCNView(frame: .zero)
        arDelegate.setARView(arView, imageToAugment: imageToAugment)
        return arView
    }
    
    func updateUIView(_ uiView: ARSCNView, context: Context) {}
}

//MARK: - Coaching animation extension

extension ARSCNView: ARCoachingOverlayViewDelegate {
    func addCoaching() {
        // Create ARCoachingOverlayView object
        let coachingOverlay = ARCoachingOverlayView()
        
        // Set the AR goal
        coachingOverlay.goal = .anyPlane
        
        // Set session
        coachingOverlay.session = self.session
        
        // Rescale when device orientation changes
        coachingOverlay.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    
        self.addSubview(coachingOverlay)
    }
}
