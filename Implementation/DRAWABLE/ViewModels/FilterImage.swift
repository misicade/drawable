//
//  FilterImageViewModel.swift
//  DRAWABLE
//
//  Handles image filtering
//
//  Created by Adela Mišicáková on 06.02.2022.
//

import SwiftUI
import CoreImage
import CoreImage.CIFilterBuiltins

class FilterImage : ObservableObject {
    
    @Published var filteredImages: [FilteredImage] = []
    
    //MARK: - Filters
    
    let filters: [CIFilter] = [
        CIFilter.photoEffectNoir(),
        CIFilter.sepiaTone(),
        CIFilter.edgeWork(),
        CIFilter.edges(),
        CIFilter.colorMonochrome(),
        CIFilter.maskToAlpha(),
        CIFilter.colorCube(),
        CIFilter.lineOverlay()
    ]
    
    //MARK: - Loading filters
    
    func loadFilter(pickedImage: UIImage) {
        // remove previously filtered images
        filteredImages.removeAll()
        
        let context = CIContext()
        
        filters.forEach{ (filter) in
            
            DispatchQueue.global(qos: .userInteractive).async {
                
                // loading image into filter
                let ciImage = CIImage(image: pickedImage)
                
                filter.setValue(ciImage!, forKey: kCIInputImageKey)
                
                // filtered image
                guard let newImage = filter.outputImage else {return}
                
                // filter name
                let filterName = filter.name.dropFirst(2)
                
                // creating UIImage
                let cgImage = context.createCGImage(newImage, from: newImage.extent)
                let filteredData = FilteredImage(image: UIImage(cgImage: cgImage!), filterName: String(filterName))
                
                DispatchQueue.main.async{
                    self.filteredImages.append(filteredData)
                }
            }
        }
    }
}
