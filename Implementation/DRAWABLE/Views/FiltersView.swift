//
//  FiltersView.swift
//  DRAWABLE
//
//  Scrollable gallery with filtered image
//
//  Created by Adela Mišicáková on 06.02.2022.
//

import SwiftUI
import CoreImage
import CoreImage.CIFilterBuiltins

struct FiltersView: View {
    
    var pickedImage: UIImage
    
    @Binding var imageToAugment: UIImage
    @StateObject var filterImagesData = FilterImage()
    
    var body: some View {
        
        let imageWidth = UIScreen.main.bounds.width/4
        let imageHeight = imageWidth
        
        VStack {
            HStack {
                
                //MARK: - Filters title label
                
                Text("Filters")
                    .fontWeight(.bold)
                    .padding()
                    .foregroundColor(Color.ui.foreground)
                    .font(.title3)
                
                Spacer()
            }
            
            //MARK: - Filters scroll gallery
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 8) {
                    
                     //MARK: - Original photo
                    
                    VStack {
                        Text("Original")
                            .font(.caption)
                        
                        Image(uiImage: pickedImage)
                            .resizable()
                            .scaledToFill()
                            .frame(width: imageWidth, height: imageHeight)
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                            .contentShape(RoundedRectangle(cornerRadius: 15))
                            .onTapGesture{
                                imageToAugment = pickedImage
                            }
                    }
                    
                   //MARK: - Filtered image previews
                    
                    ForEach(filterImagesData.filteredImages) { filteredImage in
                        VStack {
                            Text(filteredImage.filterName)
                                .font(.caption)
                            Image(uiImage: filteredImage.image)
                                .resizable()
                                .scaledToFill()
                                .background(Color(red: 0.3, green: 0.3, blue: 0.3))
                                .frame(width: imageWidth, height: imageHeight)
                                .clipShape(RoundedRectangle(cornerRadius: 15))
                                .contentShape(RoundedRectangle(cornerRadius: 15))
                                .onTapGesture{
                                    imageToAugment = filteredImage.image
                                }
                        }
                    }
                }
            }
            .padding([.leading, .bottom, .trailing])
            .onAppear {
                filterImagesData.loadFilter(pickedImage: pickedImage)
            }
        }
    }
}
