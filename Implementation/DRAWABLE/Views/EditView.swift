//
//  EditView.swift
//  DRAWABLE
//
//  A view where user edits chosen photo using filters
//
//  Created by Adela Mišicáková on 05.02.2022.
//

import SwiftUI

struct EditView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State private var showingDrawView = false
    @State var imageToAugment: UIImage
    
    let imageAuthorUrl: String
    let imageAuthorName: String
    
    var pickedImage: UIImage
    
    //MARK: - Back NavigationBar button
    //Own prettier leading back button
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        Image(systemName: "chevron.backward")
            .aspectRatio(contentMode: .fit)
            .foregroundColor(Color.ui.foreground)
    }
    }
    
    //MARK: - Draw NavigationBar button
    //Own prettier trailing draw button
    
    var drawButton : some View { Button(action: {
        self.showingDrawView = true
    }) {
        NavigationLink("", destination: DrawView(imageToAugment: imageToAugment),
                       isActive: self.$showingDrawView)
        Text("Draw")
            .foregroundColor(.white)
            .fontWeight(.bold)
            .font(.body)
            .padding([.top, .bottom], 5)
            .padding([.leading,.trailing], 10)
            .background {
                Color.ui.accent
                    .mask(RoundedRectangle(cornerRadius: 15))
            }
    }
    }
    
    var body: some View {
        ZStack {
            
            //MARK: - Background
            
            Color.ui.background
                .ignoresSafeArea(.all)
            
            VStack {
                
                //MARK: - Preview with filtered photo
                
                Rectangle()
                    .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/1.8, alignment: .center)
                    .foregroundColor(Color(red: 0.3, green: 0.3, blue: 0.3))
                    .overlay {
                        Image(uiImage: imageToAugment)
                            .resizable()
                            .scaledToFit()
                    }
                
                //MARK: - Unsplash info
                //Info about Unsplash according to Unsplash API Guidelines
                
                if !imageAuthorUrl.isEmpty {
                    let authorUrl = "\(imageAuthorUrl)?utm_source=Drawable&utm_medium=referral"
                    
                    Text("Photo by ")
                        .font(.caption)
                    + Text(.init("[\(imageAuthorName)](\(authorUrl))"))
                        .underline()
                        .font(.caption)
                    + Text(" on ")
                        .font(.caption)
                    + Text("[Unsplash](https://unsplash.com/?utm_source=Drawable&utm_medium=referral)")
                        .underline()
                        .font(.caption)
                }
                
                Spacer()
                
                //MARK: - Gallery with provided filters
                
                FiltersView(pickedImage: pickedImage, imageToAugment: $imageToAugment)
                
                Spacer()
            }
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton, trailing: drawButton)
    }
}

struct EditView_Previews: PreviewProvider {
    static var previews: some View {
        EditView(imageToAugment: UIImage(systemName: "camera")!, imageAuthorUrl: "", imageAuthorName: "", pickedImage: UIImage(systemName: "camera")!)
    }
}
