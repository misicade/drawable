//
//  GradientView.swift
//  DRAWABLE
//
//  Adds Unsplash info on top of images from provided galleries
//
//  Contains:
//      - text with author's name from Unsplash API
//      - number of likes in Unsplash API
//      - gradient for better readability
//
//  Created by Adela Mišicáková on 27.04.2022.
//

import SwiftUI

struct GradientTextOverlay: View {
    
    let likes: Int
    let photographer: String
    
    var body: some View {
        ZStack {
            
            //MARK: - Background gradient
            //For better readability
            
            LinearGradient(colors: [.black, .clear, .black], startPoint: .top, endPoint: .bottom)
            
            VStack {
                let photographerUpperCase = photographer.uppercased()
                
                //MARK: - Photographer name
                //Info about Unsplash according to Unsplash API Guidelines
                
                HStack {
                    Text(photographerUpperCase)
                        .font(.caption)
                        .bold()
                        .foregroundColor(Color.white)
                    
                    Spacer()
                }
                .padding(15)
                
                Spacer()
                
                //MARK: - Likes
                //Future feature – how many people liked this photo in this app
                
                HStack {
                    Spacer()
                    
                    Text("\(likes)")
                        .font(.caption)
                        .bold()
                        .foregroundColor(Color.white)
                    
                    Image(systemName: "heart.fill")
                        .foregroundColor(Color.white)
                }
                .padding()
            }
        }
    }
}

struct GradientTextOverlay_Previews: PreviewProvider {
    static var previews: some View {
        GradientTextOverlay(likes: 0, photographer: "Unknown")
    }
}
