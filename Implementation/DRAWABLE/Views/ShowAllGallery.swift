//
//  ShowAllGallery.swift
//  DRAWABLE
//
//  Scrollable grid with fetched photos
//
//  Created by Adela Mišicáková on 19.03.2022.
//

import SwiftUI

struct ShowAllGallery: View {
    
    @Binding var pickedImage: UIImage
    @Binding var imageAuthorUrl: String
    @Binding var imageAuthorName: String
    @Binding var imageIsChosen: Bool
    @Binding var userGallery: [UIImageWithAuthorData]
    
    let photos: [Photo]
    
    var body: some View {
        
        //MARK: - Scrollable grid gallery with two columns
        // Previously -- LazyGrid, but it was not working correctly with AsyncImage
        
        ScrollView {
            let numOfPhotos = photos.count
            
            HStack {
                Spacer()
                
                //MARK: - First column
                
                ShowAllGalleryColumn(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, photos: photos, begin: 0, end: numOfPhotos/2)
                
                Spacer()
                
                //MARK: - Second column
                
                ShowAllGalleryColumn(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, photos: photos, begin: numOfPhotos/2, end: numOfPhotos)
                
                Spacer()
            }
        }
    }
}

