//
//  GalleryView.swift
//  DRAWABLE
//
//  Scrollable gallery for photos from Unsplash API
//
//  Created by Adela Mišicáková on 03.01.2022.
//

import SwiftUI

struct GalleryView: View {
    
    @Binding var pickedImage: UIImage
    @Binding var imageAuthorUrl: String
    @Binding var imageAuthorName: String
    @Binding var imageIsChosen: Bool
    @Binding var userGallery: [UIImageWithAuthorData]
    
    @ObservedObject var photosManager = PhotosManager()
    
    let searchWord: String
    let title: String
    
    //MARK: - Image Downloading
    //Creates UIImage from url
    
    func imageDownloading(photo: Photo) {
        let photoUrl = photo.urls.small_s3
        
        DispatchQueue.global().async {
            let url = URL(string: photoUrl)!
            
            do {
                let data = try Data(contentsOf: url)
                
                DispatchQueue.main.async {
                    if let safeImage = UIImage(data: data) {
                        self.pickedImage = safeImage
                        self.imageIsChosen = true
                        
                        //Limit user gallery
                        if self.userGallery.count == 5 {
                            self.userGallery.removeFirst()
                        }
                        
                        //Save info about image for later usage
                        let photoWithAuthor = UIImageWithAuthorData(image: safeImage, authorLink: photo.user.links.html, authorName: photo.user.name)
                        self.userGallery.append(photoWithAuthor)
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    var body: some View {
        var alreadyFetched = false
        
        VStack {
            HStack {
                
                //MARK: - Gallery title label
                
                Text(title)
                    .fontWeight(.bold)
                    .padding()
                    .foregroundColor(Color.ui.foreground)
                    .font(.title2)
                
                Spacer()
                
                //MARK: - Show all button
                
                //NavigationLink to ShowAllView
                NavigationLink(destination: ShowAllView(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, searchWord: searchWord)) {
                    Text("Show all")
                        .fontWeight(.bold)
                        .font(.caption)
                        .padding(10)
                        .foregroundStyle(.white)
                        .background {
                            LinearGradient(
                                colors: [Color.ui.accent],
                                startPoint: .leading,
                                endPoint: .trailing
                            )
                            .mask(RoundedRectangle(cornerRadius: 15))
                        }
                }
                .padding()
                
            }
            .onAppear {
                if alreadyFetched == false {
                    photosManager.fetchPhoto(photoName: searchWord, numOfPhotos: 5)
                    alreadyFetched = true
                }
            }
            
            //MARK: - Gallery with photos
            
            if let safePhotos = photosManager.photos {
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack(spacing: 8) {
                        
                        let numOfPhotos = safePhotos.count
                        let imageWidth = UIScreen.main.bounds.width/3
                        let imageHeight = UIScreen.main.bounds.height/4
                        
                        ForEach(0..<numOfPhotos, id:\.self) { id in
                            
                            let photoUrl = safePhotos[id].urls.small_s3
                            let photoLikes = safePhotos[id].likes
                            let photographer = safePhotos[id].user.name
                            
                            //iOS15
                            AsyncImage(
                                url: URL(string: photoUrl),
                                content: { image in
                                    image
                                        .resizable()
                                        .scaledToFill()
                                        .frame(width: imageWidth, height: imageHeight)
                                        .clipped()
                                        .overlay(
                                            GradientTextOverlay(likes: photoLikes, photographer: photographer)
                                        )
                                        .clipShape(RoundedRectangle(cornerRadius: 15))
                                        .contentShape(RoundedRectangle(cornerRadius: 15))
                                        .onTapGesture {
                                            imageDownloading(photo: safePhotos[id])
                                            imageAuthorUrl = safePhotos[id].user.links.html
                                            imageAuthorName = photographer
                                        }
                                },
                                placeholder: {
                                    //Shows progressView while waiting for downloaded image
                                    ProgressView()
                                }
                            )
                        }
                    }
                }
                .padding(.leading)
            }
        }
    }
}
