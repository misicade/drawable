//
//  OwnGalleryView.swift
//  DRAWABLE
//
//  Gallery with already chosen/augmented photos
//  Currently limited up to 5 photos
//
//  Created by Adela Mišicáková on 03.01.2022.
//

import SwiftUI

struct OwnGalleryView: View {
    
    let title: String
    
    @Binding var pickedImage: UIImage
    @Binding var imageAuthorUrl: String
    @Binding var imageAuthorName: String
    @Binding var imageIsChosen: Bool
    @Binding var userGallery: [UIImageWithAuthorData]
    @Binding var isShowingPhotoPicker: Bool
    
    var body: some View {
        
        let imageWidth = UIScreen.main.bounds.width/5
        let imageHeight = UIScreen.main.bounds.height/5
        
        VStack {
            
            //MARK: - Gallery title label
            
            HStack {
                Text(title)
                    .fontWeight(.bold)
                    .padding()
                    .foregroundColor(Color.ui.foreground)
                    .font(.title2)
                
                Spacer()
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 10) {
                    
                    //MARK: - Camera button
                    //User can choose an image from the device gallery
                    
                    Image(systemName: "camera.fill")
                        .resizable()
                        .scaledToFit()
                        .padding(25)
                        .foregroundColor(Color.ui.foreground)
                        .frame(width: imageWidth, height: imageHeight)
                        .background(.ultraThinMaterial)
                        .clipShape(RoundedRectangle(cornerRadius: 15))
                        .onTapGesture {
                            isShowingPhotoPicker = true
                        }
                    
                    //MARK: - Already selected/augmented photos gallery
                    
                    if !userGallery.isEmpty {
                        ForEach((0..<userGallery.count).reversed(), id: \.self) { index in
                            HStack {
                                Image(uiImage: userGallery[index].image)
                                    .resizable()
                                    .scaledToFill()
                                    .frame(width: imageWidth, height: imageHeight)
                                    .clipShape(RoundedRectangle(cornerRadius: 15))
                                    .contentShape(RoundedRectangle(cornerRadius: 15))
                                    .onTapGesture {
                                        pickedImage = userGallery[index].image
                                        imageAuthorName = userGallery[index].authorName
                                        imageAuthorUrl = userGallery[index].authorLink
                                        imageIsChosen = true
                                    }
                            }
                        }
                    }
                }
                .padding(.leading)
            }
        }
    }
}
