//
//  ContentView.swift
//  DRAWABLE
//
//  Main view that holds:
//  - user gallery with already chosen/augmented photos
//  - galleries provided by app (Unsplash API)
//  - button that navigates user to device gallery
//  - button that navigates user to search the image using terms
//
//  Created by Adela Mišicáková on 03.01.2022.
//

import SwiftUI

struct ContentView : View {
    
    @State private var isShowingPhotoPicker = false
    @State private var photoSearch = false
    @State private var pickedImage = UIImage()
    @State private var imageAuthorUrl = String()
    @State private var imageAuthorName = String()
    @State private var imageIsChosen = false
    @State private var userGallery: [UIImageWithAuthorData] = []
    
    var body: some View {
        NavigationView {
            ZStack {
                
                //MARK: - Background Gradient
                
                LinearGradient(
                    gradient: Gradient(colors: [Color.ui.accent, Color.ui.background, Color.ui.background, Color.ui.background, Color.ui.background, Color.ui.background, Color.ui.background]),
                    startPoint: .topLeading,
                    endPoint: .bottomTrailing
                )
                .ignoresSafeArea()
                
                
                //MARK: - Navigation links
                
                //NavigationLink to EditView
                NavigationLink("", destination: EditView(imageToAugment: pickedImage, imageAuthorUrl: imageAuthorUrl, imageAuthorName: imageAuthorName, pickedImage: pickedImage),
                               isActive: $imageIsChosen)
    
                //NavigationLink to ShowAllView
                NavigationLink("", destination: ShowAllView(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, searchWord: "monkey"),
                               isActive: $photoSearch)
                
                ZStack {
                    ScrollView (showsIndicators: false) {
                        VStack {
                            HStack {
                                
                                //MARK: - Drawable label
                                
                                Text("DRAWABLE")
                                    .fontWeight(.bold)
                                    .font(.title3)
                                    .padding()
                                    .foregroundStyle(Color.ui.foreground)
                                
                                Spacer()
                                
                                //MARK: - Search button
                                
                                Image(systemName: "magnifyingglass")
                                    .resizable()
                                    .padding(18)
                                    .foregroundColor(Color.ui.foreground)
                                    .frame(width: UIScreen.main.bounds.height/15, height: UIScreen.main.bounds.height/15)
                                    .onTapGesture {
                                        photoSearch = true
                                    }
                                    .padding()
                                
                            }
                            .padding(.top)
                            
                            //MARK: - Title label
                            
                            HStack {
                                Text("""
                                        Hello!
                                        Let's draw today 🎨
                                        """)
                                .fontWeight(.bold)
                                .font(.largeTitle)
                                .foregroundStyle(Color.ui.foreground)
                                .fixedSize(horizontal: false, vertical: true)
                                .padding(.leading)
                                
                                Spacer()
                            }
                            
                            //MARK: - Galleries
                            
                            // User gallery
                            if userGallery.count != 0 {
                                OwnGalleryView(title: "Previously made 🎉", pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, isShowingPhotoPicker: $isShowingPhotoPicker)
                            }
                            
                            // First provided gallery
                            GalleryView(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, searchWord: "animal", title: "Choose from our perfect zoo 🐙")
                            
                            // Second provided gallery
                            GalleryView(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, searchWord: "monstera", title: "Look at those monsteras 🤫")
                            
                            // Third provided gallery
                            GalleryView(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, searchWord: "rose", title: "Roses are red, ... 🌹")
                        }
                        .padding(.bottom, 80)
                        .padding(.top, 30)
                    }
                    .ignoresSafeArea(.all)
                    
                    //MARK: - Add Button
                    //Opens device gallery from which the user can select an image
                    
                    VStack {
                        Spacer()
                            let imageWidth =  UIScreen.main.bounds.height/15
                            let imageHeight = UIScreen.main.bounds.height/15
                        
                            Image(systemName: "plus.circle.fill")
                                .resizable()
                                .frame(width: imageWidth, height: imageHeight)
                                .foregroundStyle(Color.white, Color.ui.accent)
                                .shadow(color: Color.black.opacity(0.8), radius: 8, x: 0, y: 0)
                                .padding()
                                .onTapGesture {
                                    isShowingPhotoPicker = true
                                }
                    }
                    .padding()
                    .ignoresSafeArea(.all)
                }
            }
            .navigationBarHidden(true)
            .sheet(isPresented: $isShowingPhotoPicker) {
                PhotoPicker(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, isShowingPhotoPicker: $isShowingPhotoPicker)
            }
        }
    }
}


#if DEBUG
struct ContentView_Previews : PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.light)
    }
}
#endif
