//
//  DrawView.swift
//  DRAWABLE
//
//  View with AR container, slider, ...
//
//  Created by Adela Mišicáková on 03.01.2022.
//

import SwiftUI

struct DrawView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @State var opacity = 1.0
    
    let imageToAugment: UIImage
    var arDelegate = ARDelegate()
    
    //MARK: - Back NavigationBar button
    //Own prettier leading back button
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        Image(systemName: "chevron.backward")
            .aspectRatio(contentMode: .fit)
            .foregroundColor(Color.ui.foreground)
    }
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                
                //MARK: - ARViewContainer
                
                ARViewContainer(imageToAugment: imageToAugment, arDelegate: arDelegate)
                    .ignoresSafeArea()
                
                //MARK: - Opacity slider
                //Sets opacity of the augmented image
                
                VStack {
                    Spacer()
                    
                    Slider(value: Binding(get: {
                        self.opacity
                    }, set: { (newVal) in
                        self.opacity = newVal
                        arDelegate.updatePlaneOpacity(imageOpacity: newVal)
                    }), in: 0.0...1.0, step: 0.1)
                    .padding()
                    .padding(.bottom, 40)
                    .accentColor(Color.ui.accent)
                }
            }
            .ignoresSafeArea()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: backButton)
        .onAppear{
            //Force no sleep while drawing
            UIApplication.shared.isIdleTimerDisabled = true
        }
        .onDisappear{
            //You can sleep now
            UIApplication.shared.isIdleTimerDisabled = false
        }
    }
}

struct DrawView_Previews: PreviewProvider {
    static var previews: some View {
        DrawView(imageToAugment: UIImage(systemName: "camera")!)
    }
}
