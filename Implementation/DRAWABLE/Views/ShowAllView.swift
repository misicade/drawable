//
//  ShowAllView.swift
//  DRAWABLE
//
//  Shows scrollable gallery with fetched photos
//  In this view the user can search for an image using a term
//
//  Created by Adela Mišicáková on 19.03.2022.
//

import SwiftUI

struct ShowAllView: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    
    @Binding var pickedImage: UIImage
    @Binding var imageAuthorUrl: String
    @Binding var imageAuthorName: String
    @Binding var imageIsChosen: Bool
    @Binding var userGallery: [UIImageWithAuthorData]
    
    @State var search: String = ""
    
    @ObservedObject var photosManager = PhotosManager()
    
    let searchWord : String
    
    //MARK: - Back NavigationBar button definition
    //Own prettier leading back button
    
    var backButton : some View { Button(action: {
        self.presentationMode.wrappedValue.dismiss()
    }) {
        Image(systemName: "chevron.backward")
            .aspectRatio(contentMode: .fit)
            .foregroundColor(Color.ui.foreground)
    }
    }
    
    var body: some View {
        VStack {
            
            if let photos = photosManager.photos {
                ShowAllGallery(pickedImage: $pickedImage, imageAuthorUrl: $imageAuthorUrl, imageAuthorName: $imageAuthorName, imageIsChosen: $imageIsChosen, userGallery: $userGallery, photos: photos)
            }
            
            //MARK: - Unsplash info (bottom)
            //Info about Unsplash according to Unsplash API Guidelines
            
            Text("Photos by ")
                .font(.caption)
            + Text("[Unsplash](https://unsplash.com/?utm_source=Drawable&utm_medium=referral)")
                .underline()
                .font(.caption)
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading:
                                HStack{
            
            //MARK: - Back button
            
            backButton
                .padding()
            
            //MARK: - Search field
            
            TextField("", text: $search)
                .placeholder(when: search.isEmpty) {
                    Text("Search")
                        .foregroundColor(.white)
                        .padding(.leading, 5)
                }
                .padding(8)
                .background(RoundedRectangle(cornerRadius: 15).fill(Color(red: 0.3, green: 0.3, blue: 0.3)))
                .foregroundColor(.white)
                .onSubmit {
                    photosManager.fetchPhoto(photoName: self.search, numOfPhotos: 10)
                }
            
            //MARK: - Search button
            
            Image(systemName: "magnifyingglass")
                .padding()
                .onTapGesture {
                    photosManager.fetchPhoto(photoName: self.search, numOfPhotos: 10)
                }
        }
            .frame(width: UIScreen.main.bounds.width)
            .onAppear {
                photosManager.fetchPhoto(photoName: searchWord, numOfPhotos: 10)
            }
        )
    }
}

//MARK: - PlaceHolder

extension View {
    func placeholder<Content: View>(
        when shouldShow: Bool,
        alignment: Alignment = .leading,
        @ViewBuilder placeholder: () -> Content) -> some View {
            
            ZStack(alignment: alignment) {
                placeholder().opacity(shouldShow ? 1 : 0)
                self
            }
        }
}
