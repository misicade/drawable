//
//  ShowAllGalleryColumn.swift
//  DRAWABLE
//
//  One column of a scrollable gallery with fetched photos
//  Uses AsyncImage
//
//  Created by Adela Mišicáková on 10.05.2022.
//

import SwiftUI

struct ShowAllGalleryColumn: View {
    
    @Binding var pickedImage: UIImage
    @Binding var imageAuthorUrl: String
    @Binding var imageAuthorName: String
    @Binding var imageIsChosen: Bool
    @Binding var userGallery: [UIImageWithAuthorData]
    
    let photos: [Photo]
    let begin: Int
    let end: Int
    
    //MARK: - Image downloading
    //Creates UIImage from url
    
    func imageDownloading(photo: Photo) {
        let photoUrl = photo.urls.small_s3
        
        DispatchQueue.global().async {
            let url = URL(string: photoUrl)!
            
            do {
                let data = try Data(contentsOf: url)
                
                DispatchQueue.main.async {
                    if let safeImage = UIImage(data: data) {
                        self.pickedImage = safeImage
                        self.imageIsChosen = true
                        
                        //Limit user gallery
                        if self.userGallery.count == 5 {
                            self.userGallery.removeFirst()
                        }
                        
                        //Save info about image for later usage
                        let photoWithAuthor = UIImageWithAuthorData(image: safeImage, authorLink: photo.user.links.html, authorName: photo.user.name)
                        self.userGallery.append(photoWithAuthor)
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
        }
    }
    
    var body: some View {
        
        let imageWidth = UIScreen.main.bounds.width/2.3
        let imageHeight = UIScreen.main.bounds.width/1.5
        
        VStack{
            ForEach(begin..<end, id:\.self) { id in
                let photoUrl = photos[id].urls.small_s3
                let photoLikes = photos[id].likes
                let photographer = photos[id].user.name
                
                //iOS15
                AsyncImage(
                    url: URL(string: photoUrl),
                    content: { image in
                        image
                            .resizable()
                            .aspectRatio(contentMode: .fill)
                            .frame(width: imageWidth, height: imageHeight)
                            .overlay(
                                GradientTextOverlay(likes: photoLikes, photographer: photographer)
                            )
                            .clipShape(RoundedRectangle(cornerRadius: 15))
                            .padding(5)
                            .onTapGesture {
                                imageDownloading(photo: photos[id])
                                imageAuthorUrl = photos[id].user.links.html
                                imageAuthorName = photographer
                            }
                        
                    },
                    placeholder: {
                        //Shows progressView while waiting for downloaded image
                        ProgressView()
                    }
                )
            }
        }
    }
}
