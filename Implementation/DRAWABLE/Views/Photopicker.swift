//
//  Photopicker.swift
//  DRAWABLE
//
//  Handles photoPicker – The user can select an image from the device gallery
//
//  Extension   of UIImage - fixRotation
//              of UIImage - downSize
//
//  Created by Adela Mišicáková on 03.01.2022.
//

import SwiftUI

struct PhotoPicker: UIViewControllerRepresentable {
    
    @Binding var pickedImage: UIImage
    @Binding var imageAuthorUrl: String
    @Binding var imageAuthorName: String
    @Binding var imageIsChosen: Bool
    @Binding var userGallery: [UIImageWithAuthorData]
    @Binding var isShowingPhotoPicker: Bool
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.allowsEditing = false
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {}
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(photoPicker: self)
    }
    
    final class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        
        let photoPicker : PhotoPicker
        
        init (photoPicker: PhotoPicker) {
            self.photoPicker = photoPicker
        }
        
        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
            if let image = info[.originalImage] as? UIImage {
                // fixes device gallery image rotation
                var fixedImage = image.fixOrientation()
                
                if fixedImage.size.width > 500 {
                    let smallImage = fixedImage.downSize()
                    fixedImage = smallImage
                }
                
                photoPicker.pickedImage = fixedImage
                photoPicker.imageAuthorUrl = ""
                photoPicker.imageAuthorName = ""
                
                let imageWithAuthor = UIImageWithAuthorData(image: fixedImage, authorLink: "", authorName: "")
                photoPicker.userGallery.append(imageWithAuthor)
                
                photoPicker.isShowingPhotoPicker = false
                photoPicker.imageIsChosen = true
            }
            picker.dismiss(animated: true)
        }
    }
}

//MARK: - UIImage rotation fixer extension
// Captured images are saved with the orientation returned by a sensor
// Solution from StackOverflow: https://stackoverflow.com/questions/46773589/swift-images-saved-to-phone-in-app-are-being-rotated-either-90-degrees-or-180
//

extension UIImage {
    func fixOrientation() -> UIImage {
        if self.imageOrientation == UIImage.Orientation.up {
            return self
        }

        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        self.draw(in: CGRect(x: 0, y: 0, width: self.size.width, height: self.size.height))
        let normalizedImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()

        return normalizedImage;
    }
}

//MARK: - DownSizeImage extension
// resizes image to smaller resolution when it's too big
// respects aspect ratio

extension UIImage {
    func downSize() -> UIImage {
        let aspectRatio = self.size.height / self.size.width
        let newWidth = 500.0
        let newHeight = newWidth * aspectRatio
        let size = CGSize(width: newWidth, height: newHeight)
        
        return UIGraphicsImageRenderer(size: size).image { _ in
            draw(in: CGRect(origin: .zero, size: size))
        }
    }
}
