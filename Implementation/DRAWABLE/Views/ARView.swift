//
//  ARView.swift
//  DRAWABLE
//
//  Created by Adela Mišicáková on 03.01.2022.
//

import SwiftUI

struct ARView: View {
    
    var body: some View {
        Text("Hello, World!")
    }
}

struct ARView_Previews: PreviewProvider {
    static var previews: some View {
        ARView()
    }
}
