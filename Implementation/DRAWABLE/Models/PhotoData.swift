//
//  PhotoData.swift
//  DRAWABLE
//
//  In this file are created structures for handling data from Unsplash API
//
//  Created by Adela Mišicáková on 19.03.2022.
//

import Foundation
import UIKit

struct PhotoData: Decodable {
    var results: [Photo]
}

struct Photo: Decodable {
    let likes: Int
    let urls: Url
    let user: User
}

struct Url: Decodable {
    let small_s3: String
}

struct User: Decodable {
    let name: String
    let links: UserLinks
}

struct UserLinks: Decodable {
    let html: String
}

struct UIImageWithAuthorData {
    let image: UIImage
    let authorLink: String
    let authorName: String
}
