//
//  FilteredImage.swift
//  DRAWABLE
//
//  In this file is specified struct that handles image with filter name
//
//  Created by Adela Mišicáková on 06.02.2022.
//

import SwiftUI

struct FilteredImage: Identifiable {
    var id = UUID().uuidString
    var image: UIImage
    var filterName: String
}
