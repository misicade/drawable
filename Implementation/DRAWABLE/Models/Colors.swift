//
//  Colors.swift
//  DRAWABLE
//
//  In this file are defined own colors as an extension of Color
//  Uses Assets and changes with color mode of the device
//
//  Created by Adela Mišicáková on 25.04.2022.
//

import Foundation
import SwiftUI

extension Color {
    static let ui = Color.UI()
    
    struct UI {
        let background = Color("background")
        let foreground = Color("foreground")
        let accent = Color("accent")
    }
}
