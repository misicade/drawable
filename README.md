# DRAWABLE :pencil2:

Implementation of a mobile application for iOS devices to facilitate drawing of images using augmented reality. The application uses ARKit framework to work with augmented reality and SwiftUI framework to implement the user interface. The result is a mobile application that allows users to select images (from their own or provided gallery) and projects them on a given vertical or horizontal surface. The image can be edited using filters. Also, it is possible to change its size, rotation, position or transparency. Options for extending the resulting application are listed in the conclusion.

### Repository Structure :card_index_dividers:

  - **[Examples](Examples)**: includes app demo video, mockups and user testing results
  - **[Implementation](Implementation)**: inlcudes source code
  
### App preview :iphone:
![Mockup1](Examples/img/Mockup1.png)
![Mockup2](Examples/img/Mockup2.png)
![Mockup3](Examples/img/Mockup3.png)
